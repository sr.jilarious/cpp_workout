// Simple linked list implementations.

#include <stdlib.h>
#include <memory>
#include <functional>
#include <list>
#include <vector>
#include <assert.h>

namespace workout
{
    ///////////////////////////////////////////////////////////////////////////
    // A node for a binary tree holding arbitrary data.
    ///////////////////////////////////////////////////////////////////////////
    template<class DataType>
    class TreeNode
    {
    public:
        
        TreeNode(DataType d)
        {
            left = nullptr;
            right = nullptr;
            data = d;
        }

        DataType data;
        std::shared_ptr<TreeNode<DataType>> left;
        std::shared_ptr<TreeNode<DataType>> right;
    };

    ///////////////////////////////////////////////////////////////////////////
    // An abstract tree data structure.
    ///////////////////////////////////////////////////////////////////////////
    template<class DataType>
    class Tree
    {
    public:
        virtual ~Tree() {}

        virtual void insert(DataType data) = 0;
        virtual bool remove(DataType data) = 0;
        virtual bool lookup(DataType data) = 0;

        virtual void dfs(std::function<void(const DataType&)> func) = 0;
        virtual void bfs(std::function<void(const DataType&)> func) = 0;
    };

    ///////////////////////////////////////////////////////////////////////////
    // A simple binary search tree.
    ///////////////////////////////////////////////////////////////////////////
    template<class DataType>
    class BinarySearchTree : public Tree<DataType>
    {
        typedef TreeNode<DataType> Node;
        typedef std::shared_ptr<Node> NodePtr;
        typedef std::function<void(const DataType&)> VisitFunc;

        NodePtr mRoot;

        void dfsCore(NodePtr node, VisitFunc func)
        {
            if (node->left != nullptr) {
                dfsCore(node->left, func);
            }

            func(node->data);

            if (node->right != nullptr) {
                dfsCore(node->right, func);
            }
        }

    public:
        virtual ~BinarySearchTree() {}

        void insert(DataType data) override
        {
            if (mRoot == nullptr) {
                mRoot = std::make_shared<Node>(data);
            }
            else {
                // Iterative dfs to find place to insert.
                std::vector<NodePtr> dfsStack;
                dfsStack.push_back(mRoot);

                while (!dfsStack.empty())
                {
                    auto node = dfsStack.back();
                    dfsStack.pop_back();
                    if (data == node->data) {
                        // Value already exists in the search tree, so leave.
                        return;
                    }
                    else if (data < node->data) {
                        if (node->left == nullptr) {
                            // Insert the node since we found its position
                            node->left = std::make_shared<Node>(data);
                            return;
                        }
                        else {
                            dfsStack.push_back(node->left);
                        }
                    }
                    else { 
                        if (node->right == nullptr) {
                            // Insert the node since we found its position
                            node->right = std::make_shared<Node>(data);
                            return;
                        }
                        else {
                            dfsStack.push_back(node->right);
                        }
                    }
                }

                assert(false);

            }
        }

        bool remove(DataType data) override
        {
            return false;
        }

        bool lookup(DataType data) override
        {
            return false;
        }

        void dfs(VisitFunc func) override
        {
            // Recursive depth-first-search
            dfsCore(mRoot, func);
        }

        void bfs(VisitFunc func) override
        {
            std::list<NodePtr> bfsQueue;

            bfsQueue.push_back(mRoot);

            while (!bfsQueue.empty())
            {
                NodePtr node = bfsQueue.front();
                bfsQueue.pop_front();
                if (node->left != nullptr) bfsQueue.push_back(node->left);
                if (node->right != nullptr) bfsQueue.push_back(node->right);

                func(node->data);
            }
        }
    };

    ///////////////////////////////////////////////////////////////////////////
    // A binary tree node that we can paint red/black
    ///////////////////////////////////////////////////////////////////////////
    template<class DataType>
    class RedBlackTreeNode 
    {
    private:
        bool mIsRed;

    public:
        bool isRed() const { return mIsRed; }
        bool isBlack() const { return !mIsRed; }

        void markRed() { mIsRed = true; }
        void markBlack() { mIsRed = false; }

        DataType data;
        std::shared_ptr<RedBlackTreeNode<DataType>> left;
        std::shared_ptr<RedBlackTreeNode<DataType>> right;
    };

    ///////////////////////////////////////////////////////////////////////////
    // A Red-Black Tree implementation.
    ///////////////////////////////////////////////////////////////////////////
    template<class DataType>
    class RedBlackTree : public Tree<DataType>
    {
    private:
        RedBlackTreeNode mRoot;

    public:
        virtual ~RedBlackTree(){}

        void insert(DataType data) override
        {

        }

        bool remove(DataType data) override
        {
            return false;
        }

        bool lookup(DataType data) override
        {
            return false;
        }
    };
};