// Simple templated merge-sort implementation

namespace workout
{
    template<typename DataType>
    class MergeSort 
    {
    public:
        static void sort(DataType arr[], int len)
        {
            auto working = new DataType[len];
            memcpy(working, arr, len*sizeof(DataType));
            sortCore(arr, 0, len-1, working);
            delete[] working;
        }

    private:
        static void sortCore(DataType arr[], int begin, int end, DataType working[])
        {
            // 1 or 0 elements means the arr array is sorted already.
            if(end-begin <= 1) 
                return;

            int mid = (end - begin) / 2;

            // sort the left half
            sortCore(arr, begin, mid, working);

            // sort the right half
            sortCore(arr, mid+1, end, working);

            // Merge the two sorted pieces from working, back into arr.
            int leftIndex = begin;
            int rightIndex = mid+1;
            for(int count = begin; count <= end; count++) {
                if(leftIndex < mid && (rightIndex > end || working[leftIndex] < working[rightIndex])) {
                    working[count] = arr[leftIndex];
                    leftIndex++;
                } else
                {
                    working[count] = arr[rightIndex];
                    rightIndex++;
                }
            }
        }
    };
}