// Simple linked list implementations.

#include <stdlib.h>
#include <memory>

namespace workout
{
    template<class DataType>
    class LinkedListNode
    {
    public:
        LinkedListNode()
        {
            next = nullptr;
            data = nullptr;
        }

        LinkedListNode(DataType d)
        {
            next = nullptr;
            data = d;
        }

        LinkedListNode(LinkedListNode<DataType>& other)
        {
            next = next;
            data = data;
        }

        LinkedListNode(LinkedListNode<DataType>&& other)
        {
            next = std::move(next);
            data = data;
        }

        std::shared_ptr<LinkedListNode<DataType>> next;
        DataType data;
    };

    template<class DataType>
    class SingleLinkedList
    {
        typedef LinkedListNode<DataType> Node;
        typedef std::shared_ptr<Node> NodePtr;

    public:
		SingleLinkedList()
		{
			head = nullptr;
		}

        NodePtr head;

        bool pushEnd(DataType data)
        {
            NodePtr node = std::make_shared<Node>(data);
            if(head == nullptr) {
                head = node;
            } 
            else {
                NodePtr listNode = head;
                while (listNode->next != nullptr) listNode = listNode->next;
                
                listNode->next = node;
            }

            return true;
        }

        DataType popEnd()
        {
            if (head == nullptr) return DataType();
            else if (head->next == nullptr) {
                auto nodePtr = head;
                head = nullptr;
                return nodePtr->data;
            }
            else {
                NodePtr node = head;

                // Get to the second to last node in the list.
                while (node->next->next != nullptr) node = node->next;

                auto dataNode = node->next;
                node->next = nullptr;
                return dataNode->data;
            }
        }

        bool pushStart(DataType data)
        {
            NodePtr node = std::make_shared<Node>(data);
            node->next = head;
            head = node;
            return true;
        }

        DataType popStart()
        {

            if (head == nullptr) return DataType();

            auto oldHead = head;
            head = oldHead->next;
            return oldHead->data;
        }

        bool isEmpty() const
        {
            return head == nullptr;
        }
    };
};