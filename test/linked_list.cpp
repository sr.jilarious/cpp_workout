
#include "lib/LinkedList.h"
#include "gtest/gtest.h"

#include <stdio.h>
#include <string>

struct TestStruct
{
    TestStruct() 
    {
        num = 0;
        str = "";
    }

    TestStruct(int n, const std::string&& s)
    {
        num = n;
        str = std::move(s);
    }

    int num;
    std::string str;
};

TEST(LinkeListTest, push_start_int_test)
{
    auto list = workout::SingleLinkedList<int>();
    ASSERT_TRUE(list.isEmpty());
    ASSERT_EQ(list.popStart(), 0);

    list.pushStart(3);
    ASSERT_FALSE(list.isEmpty());

    auto outPtr = list.popStart();
    ASSERT_TRUE(list.isEmpty());

    ASSERT_EQ(outPtr, 3);
}

TEST(LinkeListTest, push_start_struct_test)
{
    auto list = workout::SingleLinkedList<TestStruct>();
    ASSERT_TRUE(list.isEmpty());
    
    list.pushStart(TestStruct(123, "Dummy"));
    ASSERT_FALSE(list.isEmpty());
    
    auto out = list.popStart();
    ASSERT_TRUE(list.isEmpty());

    ASSERT_EQ(out.num, 123);
    ASSERT_EQ(out.str, "Dummy");
}

TEST(LinkeListTest, push_end_struct_test)
{
    auto list = workout::SingleLinkedList<TestStruct>();
    ASSERT_TRUE(list.isEmpty());

    list.pushEnd(TestStruct(123, "Dummy"));
    ASSERT_FALSE(list.isEmpty());

    auto out = list.popEnd();
    ASSERT_TRUE(list.isEmpty());

    ASSERT_EQ(out.num, 123);
    ASSERT_EQ(out.str, "Dummy");

    list.pushEnd(TestStruct(234, "abc"));
    list.pushEnd(TestStruct(345, "def"));
    list.pushEnd(TestStruct(456, "ghi"));

    ASSERT_FALSE(list.isEmpty());
    
    out = list.popEnd();
    ASSERT_EQ(out.num, 456);
    ASSERT_EQ(out.str, "ghi");

    out = list.popEnd();
    ASSERT_EQ(out.num, 345);
    ASSERT_EQ(out.str, "def");

    out = list.popEnd();
    ASSERT_EQ(out.num, 234);
    ASSERT_EQ(out.str, "abc");
}
