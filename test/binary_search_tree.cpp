#include "lib/Tree.h"
#include "gtest/gtest.h"

#include <stdio.h>
#include <string>

using namespace workout;

TEST(BinarySearchTreeTest, simple_test)
{
    BinarySearchTree<int> bst;
    bst.insert(10);
    bst.insert(5);
    bst.insert(3);
    bst.insert(2);
    bst.insert(4);
    bst.insert(12);
    bst.insert(11);

    printf("DFS: ");
    bst.dfs([](const int& i) {
        printf("%d, ", i);
    });

    printf("\nBFS: ");
    bst.bfs([](const int& i) {
        printf("%d, ", i);
    });

    printf("\ndone\n\n");
}