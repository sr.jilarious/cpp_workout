
#include <vector>
#include <algorithm>
#include <cstdio>
#include <functional>
#include <string>

struct Data {
    std::string name;
    bool selected;

    bool operator<(const Data& b); 
};

bool Data::operator<(const Data& b)
{
    return name < b.name;
}

void printVec(std::vector<Data> vec) {
    printf("--------------------\n");
    std::for_each(std::begin(vec), std::end(vec), [] (auto it) {
        printf("%s\n", it.name.c_str());
    });
    printf("\n\n");
}

int main()
{
    printf("Testing partition for use case of moving selection of items.\n");

    std::vector<Data> vec = {
        {"Milk", false},
        {"Jerky", false},
        {"M & Ms", true},
        {"Diet Coke", true},
        {"Potato Chips", false},
        {"Lunch meat", false},
        {"Soup", true},
        {"Chicken", false}
    };

    std::rotate(std::begin(vec), std::begin(vec) + 4, std::end(vec));
    printVec(vec);

    std::rotate(std::rbegin(vec), std::rbegin(vec) + 4, std::rend(vec));
    printVec(vec);
    
    // auto comp = [] (Data& it) -> bool {
    //     return it.selected;
    // };

    auto p = std::begin(vec) + 4;
    std::stable_partition(std::begin(vec), p, [] (Data& it) -> bool {
        return !it.selected;
    });
    std::stable_partition(p, std::end(vec), [] (Data& it) -> bool {
        return it.selected;
    });
    printVec(vec);
}
