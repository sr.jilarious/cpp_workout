
#include "gtest/gtest.h"

#include <stdio.h>
#include <string>

// ----------------------------------------------------------------------------
// Check string has all unique chars
// ----------------------------------------------------------------------------
bool checkUnique(std::string str) {
    if(str.length() <= 1) return true;

    std::sort(str.begin(), str.end());
    for(int i = 1; i < str.length(); i++) {
        if(str[i] == str[i-1]) return false;
    }

    return true;
}


TEST(AlgoTest, check_unique_test)
{
    ASSERT_TRUE(checkUnique(""));
    ASSERT_TRUE(checkUnique("a"));
    ASSERT_TRUE(checkUnique("ab"));
    ASSERT_TRUE(checkUnique("abc"));

    ASSERT_FALSE(checkUnique("aa"));
    ASSERT_FALSE(checkUnique("aba"));
}

// ----------------------------------------------------------------------------
// Check if two strings are algorithms
// ----------------------------------------------------------------------------
bool areAnagrams(std::string a, std::string b) 
{
    if(a.length() != b.length()) return false;
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());

    for(int i = 0; i < a.length(); i++) {
        if(a[i] != b[i]) return false;
    }

    return true;
}

TEST(AlgoTest, check_anagram_test)
{
    ASSERT_TRUE(areAnagrams("a", "a"));
    ASSERT_TRUE(areAnagrams("ab", "ba"));
    ASSERT_TRUE(areAnagrams("cab", "bac"));
    
    ASSERT_FALSE(areAnagrams("abcd", "bca"));
    ASSERT_FALSE(areAnagrams("abcd", "bcae"));
    
}

// ----------------------------------------------------------------------------
// Rotate an NxN array in place.
// ----------------------------------------------------------------------------
inline int getIndex(int i, int j, int n)
{
    return j*n + i;
}

void rot90Inplace(int arr[], int n) 
{
    if (n <= 1) return;

    int halfSize = (n+1)/2;

    for(int j = 0; j < halfSize; j++) {
        for(int  count = 0; count < (n-1)-2*j; count++) {
            int tlIndex = getIndex(j+count, j, n);
            int trIndex = getIndex(n-j-1, j+count, n);
            int blIndex = getIndex(j, n-j-1-count, n);
            int brIndex = getIndex(n-j-1 - count, n-j-1, n);

            int temp = arr[tlIndex];
            arr[tlIndex] = arr[trIndex];
            arr[trIndex] = arr[brIndex];
            arr[brIndex] = arr[blIndex];
            arr[blIndex] = temp;
        }
    }
}

TEST(AlgoTest, rot90_test1) 
{
    int arr[] = { 0 };

    rot90Inplace(arr, 1);

    int expected[] = { 0 };
    ASSERT_TRUE(memcmp(arr, expected, sizeof(arr)) == 0);
}

TEST(AlgoTest, rot90_test2)
{
    int arr[] = { 0, 1,
                  2, 3 };

    rot90Inplace(arr, 2);

    int expected[] = { 1, 3,
                       0, 2 };
    ASSERT_TRUE(memcmp(arr, expected, sizeof(arr)) == 0);
}

TEST(AlgoTest, rot90_test3) 
{
    int arr[] = { 0, 1, 2,
                  3, 4, 5,
                  6, 7, 8};

    rot90Inplace(arr, 3);

    int expected[] = {
        2, 5, 8,
        1, 4, 7,
        0, 3, 6
    };

    ASSERT_TRUE(memcmp(arr, expected, sizeof(arr)) == 0);
}


TEST(AlgoTest, rot90_test4)
{
    int arr[] = { 0, 1, 2, 3,
                  4, 5, 6, 7,
                  8, 9, 10, 11,
                  12, 13, 14, 15};

    rot90Inplace(arr, 4);

    int expected[] = {
        3, 7, 11, 15,
        2, 6, 10, 14,
        1, 5, 9, 13,
        0, 4, 8, 12,
    };

    ASSERT_TRUE(memcmp(arr, expected, sizeof(arr)) == 0);
}

TEST(AlgoTest, rot90_test5)
{
    int arr[] = { 0, 1, 2, 3, 4,
                  5, 6, 7, 8, 9, 
                  10, 11, 12, 13, 14,
                  15, 16, 17, 18, 19,
                  20, 21, 22, 23, 24};

    rot90Inplace(arr, 5);

    int expected[] = {
        4, 9, 14, 19, 24,
        3, 8, 13, 18, 23,
        2, 7, 12, 17, 22,
        1, 6, 11, 16, 21,
        0, 5, 10, 15, 20,
    };

    ASSERT_TRUE(memcmp(arr, expected, sizeof(arr)) == 0);
}