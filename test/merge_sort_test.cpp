
#include "gtest/gtest.h"
#include "lib/MergeSort.h"

using namespace workout;

TEST(MergeSortTest, merge_sort_test_1)
{
    const int len = 2;
    int arr[] = { 3, 1 };
    MergeSort<int>::sort(arr, len);

    int expected[] = { 1, 3 };
    ASSERT_TRUE(memcmp(arr, expected, len) == 0);
}

TEST(MergeSortTest, merge_sort_test_2)
{
    const int len = 5;
    int arr[] = { 3, 1, 9, 8, 7 };
    MergeSort<int>::sort(arr, len);

    int expected[] = { 1, 3, 7, 8, 9 };
    ASSERT_TRUE(memcmp(arr, expected, len) == 0);
}