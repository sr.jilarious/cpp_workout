#include <vector>
#include <iostream>

int main() {
    std::vector< int > vec{1,2,3,4,5};

    auto mvec = std::move(vec);

    std::cout << vec.size() << std::endl;

    std::cout << mvec.size() << std::endl;
}
